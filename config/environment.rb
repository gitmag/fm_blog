# Load the Rails application.
require_relative 'application'

RFM_CONFIG = {:use => :development}

# Initialize the Rails application.
Rails.application.initialize!
