Rails.application.routes.draw do

    # routes!
    get 'log_in' => 'sessions#new', :as => 'log_in'
    get 'sign_up' => 'users#new', :as => 'sign_up'
    get 'log_out' => 'sessions#destroy', :as => 'log_out'

    # resources!
    resources :posts do
        resources :comments, :except => [:edit, :update]
    end
    resources :users, :except => [:index, :destroy]
    resources :sessions, :only => [:new, :create, :destroy]

    # root '/' path!
    root :to => 'welcome#index'
end
