# README

This is a blog/post website built on ruby/rails and using Filemaker as a database.
It communicates with Filemaker using the [RFM](https://github.com/ginjo/rfm/) gem from ginjo

Things missing from the source:

+ config/rfm.yml : Used to configure the Filemaker database connection
+ config/secrets.yml : secret?
