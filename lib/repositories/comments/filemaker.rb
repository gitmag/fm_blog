module Repositories
  module Comments
    class Filemaker < BaseRepository
      def model_class
        Comment
      end

      def new(attributes = {})
        @post = model_class.new(attributes)
        @post.id = next_id
        @post
      end

      def save(post)
        @id = post.save
      end

      def find_by_id(id)
        @post = model_class.find(id)
      end

      def query(options={})
        # ???
      end
    end
  end

end