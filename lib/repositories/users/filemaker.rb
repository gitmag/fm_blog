module Repositories
  module Users
    class Filemaker < BaseRepository
      def model_class
        User
      end

      def new(attributes = {})
        @user = model_class.new(attributes)
        @user.id = next_id
        @user
      end

      def save(user)
        @id = user.save
      end

      def find_by_id(id)
        @user = model_class.find(id)
      end

      def query(options={})
        # ???
      end
    end

  end

end