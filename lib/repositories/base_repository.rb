require 'securerandom'

module Repositories
  class BaseRepository

    private

    def next_id
      SecureRandom.uuid
    end
  end
end
