class Comment < Rfm::Base
  config :layout=>'Comments',
         :field_mapping => { 'idPost' => 'id_post',
                             'idUser' => 'id_user',
                             'CreationTimestamp' => 'creation_time',
                             'Body' => 'body',
                             'users_POSTS_comments_USER::DisplayName' => 'display_name'}

end
