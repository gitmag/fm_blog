class Post < Rfm::Base
  attr_accessor :comments

  config :layout=>'AllPosts',
         :field_mapping => {'id' => 'id',
             'Title' => 'title',
             'Body' => 'body',
             'creationTimestamp' => 'creation_time',
             'idUser' => 'id_user',
             'Users::DisplayName' => 'display_name'}
  validates :title, length: {minimum: 2}
  validates :body, length: {minimum: 5}

end
