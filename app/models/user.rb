class User < Rfm::Base
    attr_accessor :password, :hidden

    before_create :encrypt_password

    config :layout=>'Users',
           :field_mapping => {'PasswordHash' => 'password_hash',
             'PasswordSalt' => 'password_salt',
             'EmailAddress' => 'email',
             'DisplayName' => 'display_name'}
    validates :email, length: {minimum: 6}
    validates :display_name, length: {minimum: 2}
    validates :password, length: {minimum: 6}
    validates :hidden, numericality: { equal_to: 3}

    # Authenticate a user
    def self.authenticate(email, password)
      # Include the == when searching for exact match because filemaker
      user = User.find(:email => '==' + email).first
      puts email
      if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
        puts 'user authenticated'
        user
      else
        puts 'user not authenticated'
        nil
      end
    end

    # Hash the provided password for storings
    def encrypt_password
      if password.present?
        puts 'password is present'
        self.password_salt = BCrypt::Engine.generate_salt
        self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
      end
    end
end
