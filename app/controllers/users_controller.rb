require 'securerandom'

class UsersController < ApplicationController
  before_action :check_logged_in, only: [:edit, :update]

  def new
    @user = User.new
  end

  def edit
    @user = User.find(:id => params[:id]).first
    authorize(@user.id, root_path)
  end

  def create
    # TODO Add password confirmation
    # TODO Refactor everything!
    # Checks for existing user with this email
    @user = User.find(:email => '==' + post_params[:email]).first
    if @user
      @user.errors.add(:base, 'A user already exists with this email address')
      render 'new' and return
    end
    @user = User.new(:email => post_params[:email], :display_name => post_params[:display_name], :password => post_params[:password], :hidden => post_params[:hidden])
    @user.id = SecureRandom.uuid
    logger.info @user
    begin
      @user.save!
      redirect_to root_path
    rescue RuntimeError
      render 'new'
    end
  end

  def update
    # TODO Set up editing of passwords, and whatever other fields should exist
    @user = User.find(:id => params[:id]).first
    authorize(@user.id, root_path)

    begin
      # This is so because the rfm OIMSDOIDSCDM calls its save with validation from its save without validation. i should just change it.
      @user.update_attributes(:email => post_params[:email], :display_name => post_params[:display_name], :password => 'random', :hidden => 3)
      # Skip validation since we don't have the password
      @user.save
      redirect_to root_path, notice: 'Your information has been updated!'
    rescue
      logger.info 'wht the fuck'
      render 'edit'
    end


  end

  private
    def post_params
      params.require(:user).permit(:email, :display_name, :password, :hidden)
    end

end
