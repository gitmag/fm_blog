class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user

  def check_logged_in
    logger.info 'checking if user is logged in'
    if session[:user_id]
      true
    else
      redirect_to posts_path, notice: 'You must be logged in'
    end
  end

  # Check that the current use is authorized to access this content
  def authorize(id, redirect_path)
    logger.info 'checking user id'
    if session[:user_id] == id
      true
    else
      redirect_to redirect_path, notice: 'You do not have access to modify this content'
    end

  end

  private
  # Gets the current user by the session's user id if it exists
    def current_user
      @current_user ||= User.find(:id => session[:user_id]).first if session[:user_id]
    end
end
