class SessionsController < ApplicationController
    def new
    end

    # Log in with the provided credentials
    def create
        user = User.authenticate(params[:email], params[:password])
        if user
            session[:user_id] = user.id
            redirect_to root_url
        else
            flash.now.alert = 'Invalid username/password'
            render 'new'
        end
    end

    # Logout by removing the user id stored in the session
    def destroy
        session[:user_id] = nil
        redirect_to root_url
    end
end
