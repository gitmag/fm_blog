require 'securerandom'

class PostsController < ApplicationController
  # TODO Refactor to using a repositories!
  # Require a logged in user for everything except ...
  before_action :check_logged_in, except: [:index, :show]

  def index
    @posts = Post.find({:id => '*'}, :sort_field => :creation_time, :sort_order => :descend)
  end

  def show
    @post = Post.find(:id => params[:id]).first
    # @post = Repository.for(:post).find_by_id(params[:id])
    # Get comments for this post
    @post.comments = Comment.find({:id_post => @post.id}, :sort_field => :creation_time, :sort_order => :descend, :max_records => 20)
    @post
  end

  def new
    @post = Post.new
  end

  def edit
    @post = Post.find({:id => params[:id]}).first
    authorize(@post.id_user, post_path)
    @post
  end

  def create
    # FFFFFFFFF y no pparams passsing wtf
    begin
      @post = Post.new :title => post_params[:title], :body => post_params[:body]
      @post.id = SecureRandom.uuid
      @post.id_user = session[:user_id]

      @post.save!
      redirect_to posts_path
    rescue RuntimeError
      render 'new'
    end
  end

  def update
    @post = Post.find(:id => params[:id]).first
    authorize(@post.id_user, post_path)

    if @post.update_attributes!(post_params)
      redirect_to @post
    else
      render 'edit'
    end
  end

  def destroy
    @post = Post.find(:id => params[:id]).first
    authorize(@post.id_user, post_path)

    # Need to delete any comments as well
    @post.destroy
    redirect_to posts_path
  end

  private
    def post_params
      params.require(:post).permit(:title, :body)
    end

end
