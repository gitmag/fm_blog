require 'securerandom'

class CommentsController < ApplicationController
  before_action :check_logged_in, except: [:index, :show]

  def index
    @comments = Comment.find({:id => '*'}, :sort_field => :creation_time)
  end

  def show
    @comment = Comment.find(:id => params[:id]).first
  end

  def new
    @comment = Comment.new
  end

  def edit
    @comment = Comment.find(:id => params[:id]).first
  end

  def create
    begin
      @comment = Comment.new(:body => post_params[:body])
      @comment.id = SecureRandom.uuid
      @comment.id_post = params[:post_id]
      @comment.id_user = session[:user_id]

      @comment.save!
      redirect_to posts_path
    rescue RuntimeError
      render 'edit'
    end
  end

  def update
  end

  def destroy
    @commment = Comment.find(:id => params[:id]).first

    @comment.destroy
    redirect_to posts_path
  end

  private
    def post_params
      params.require(:comment).permit(:body)
    end
end
