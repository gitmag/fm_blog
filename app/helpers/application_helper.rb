module ApplicationHelper
  def check_for_user
    true
  end

  # Formats filemaker timestamps into something more presentable
  def format_date(date)
    date.strftime('%x %r')
  end
end
